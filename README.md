# options

This library provides an extensible command line arguments parser.

## Licence

The package is licensed under the BSD-2-Clause license; for details, see the
[LICENSE](/LICENSE) file.
